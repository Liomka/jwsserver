import javax.json.Json;
import javax.json.JsonArrayBuilder;

import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.websocket.Session;
import javax.websocket.EncodeException;
import javax.websocket.OnMessage;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

@ServerEndpoint("/")
public class WebSocketChat {

    // ------------------------------------------------------------------------
    // Don't loose me this
    // ------------------------------------------------------------------------
    static private Map<Session, ChatUser > connectedList = new HashMap<Session, ChatUser>();


    // ------------------------------------------------------------------------
    // WebSocket system
    // ------------------------------------------------------------------------
	@OnMessage
    public void onMessage(String message, Session session)
            throws IOException, InterruptedException, EncodeException {

        if (message.startsWith("/nick ")) {
            connectedList.get(session).setName(message.substring(6));
            notifyUserchangeName(session);
        } else {
            sendMessageToAll(session, message);
        }
    }
	
	@OnOpen
    public void onOpen (Session session) throws IOException, EncodeException {
        connectedList.put(session, new ChatUser(session));
        sendSystemMessageToUser(session, "Bonjour " + connectedList.get(session).name() + ". /nick name if you have a \"name\"");
        sendUserList(session);
        notifyUserConnected(session);
    }

    @OnClose
    public void onClose (Session session) throws IOException, EncodeException {
        notifyUserdisconnected(session);
        connectedList.remove(session);

    }

    // ------------------------------------------------------------------------
    // High Quality Speakers
    // ------------------------------------------------------------------------
    private void sendMessageToUser(Session from, Session to, String msg) throws IOException, EncodeException {
        JsonObjectBuilder builder = Json.createObjectBuilder()
                .add("message", Json.createObjectBuilder()
                        .add("content", msg)
                        .add("from", connectedList.get(from).toJson()));
        to.getBasicRemote().sendObject(builder.build());
    }

    private void sendMessageToAll(Session from, String msg) throws IOException, EncodeException {
        for (Entry<Session, ChatUser > entry : connectedList.entrySet())
            sendMessageToUser(from, entry.getKey(), msg);
    }

    private void sendSystemMessageToUser(Session to, String msg) throws IOException, EncodeException {
        JsonObjectBuilder builder = Json.createObjectBuilder()
                .add("systemMessage", Json.createObjectBuilder()
                        .add("content", msg));
        to.getBasicRemote().sendObject(builder.build());
    }

    private void sendSystemMessageToAll(String msg) throws IOException, EncodeException {
        for (Entry<Session, ChatUser > entry : connectedList.entrySet())
            sendSystemMessageToUser(entry.getKey(), msg);
    }

    // ------------------------------------------------------------------------
    // Sending objects
    // ------------------------------------------------------------------------
    private void sendObjectToUser(JsonObject jsonObject, Session user) throws IOException, EncodeException {
        user.getBasicRemote().sendObject(jsonObject);
    }

    private void sendObjectToAll(JsonObject jsonObject) throws IOException, EncodeException {
        for (Entry<Session, ChatUser > entry : connectedList.entrySet())
            sendObjectToUser(jsonObject, entry.getKey());
    }

    private void sendObjectToAllExceptUser(JsonObject jsonObject, Session user) throws IOException, EncodeException {
        for (Entry<Session, ChatUser > entry : connectedList.entrySet())
            if (entry.getKey() != user)
                sendObjectToUser(jsonObject, entry.getKey());
    }

    // ------------------------------------------------------------------------
    // User notifications system
    // ------------------------------------------------------------------------
    private void sendUserList(Session to) throws IOException, EncodeException {
        JsonArrayBuilder userListbuilder = Json.createArrayBuilder();
        for (Entry<Session, ChatUser > entry : connectedList.entrySet())
            userListbuilder.add(entry.getValue().toJson());
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("userList", userListbuilder);
        sendObjectToUser(builder.build(), to);
    }

    private void notifyUserConnected(Session connectedUser) throws IOException, EncodeException {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("userConnected", connectedList.get(connectedUser).toJson()).build();
        sendObjectToAllExceptUser(jsonObject, connectedUser);
    }

    private void notifyUserdisconnected(Session disconnectedUser) throws IOException, EncodeException {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("userDisconnected", connectedList.get(disconnectedUser).toJson()).build();
        sendObjectToAllExceptUser(jsonObject, disconnectedUser);
    }

    private void notifyUserchangeName(Session session) throws IOException, EncodeException {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("userChangeName", connectedList.get(session).toJson()).build();
        sendObjectToAll(jsonObject);
    }
}
