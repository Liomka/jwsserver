import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import javax.websocket.Session;

/**
 * Created by Raymond Barre on 03/06/2014.
 */

public class ChatUser {

    private String _name;
    private Session _session;

    // ------------------------------------------------------------------------
    // CSTRs
    // ------------------------------------------------------------------------
    public ChatUser(Session session) {
        _name = "Régis";
        _session = session;
    }

    public ChatUser(String name, Session session) {
        _name = name;
        _session = session;
    }

    // ------------------------------------------------------------------------
    // Accessors
    // ------------------------------------------------------------------------
    public String name() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public Session session() {
        return _session;
    }

    // ------------------------------------------------------------------------
    // Converters
    // ------------------------------------------------------------------------
    public JsonObject toJson() {
        JsonObjectBuilder jsonObject = Json.createObjectBuilder();
        jsonObject.add("name", name())
                .add("id", session().getId());
        return jsonObject.build();
    }
}
